# Install script for directory: C:/bsf-release-v1.0.0/Source

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files/bsf")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/" TYPE DIRECTORY FILES "C:/bsf-release-v1.0.0/Source/../Data" REGEX "/\\.\\.\\/data\\/raw$" EXCLUDE)
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("C:/bsf-release-v1.0.0/Source/Foundation/cmake_install.cmake")
  include("C:/bsf-release-v1.0.0/Source/Plugins/bsfD3D11RenderAPI/cmake_install.cmake")
  include("C:/bsf-release-v1.0.0/Source/Plugins/bsfOpenAudio/cmake_install.cmake")
  include("C:/bsf-release-v1.0.0/Source/Plugins/bsfRenderBeast/cmake_install.cmake")
  include("C:/bsf-release-v1.0.0/Source/Plugins/bsfPhysX/cmake_install.cmake")
  include("C:/bsf-release-v1.0.0/Source/Plugins/bsfFBXImporter/cmake_install.cmake")
  include("C:/bsf-release-v1.0.0/Source/Plugins/bsfFontImporter/cmake_install.cmake")
  include("C:/bsf-release-v1.0.0/Source/Plugins/bsfFreeImgImporter/cmake_install.cmake")
  include("C:/bsf-release-v1.0.0/Source/Plugins/bsfSL/cmake_install.cmake")

endif()

